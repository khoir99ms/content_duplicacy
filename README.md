## Installing

* Go to the directory in which you want to install, for example: `/var/www/html`:

```bash
cd /var/www/html
```

* Clone project by Git:

```bash
git clone https://bitbucket.org/khoir99ms/content_duplicacy.git
```

* Access the project page:

```
http://localhost/content_duplicacy
```