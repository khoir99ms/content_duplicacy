<?php
    define('CHUNK_SIZE', 1024 * 10); // Read Only 10kb

    function find_all_files($dir)
    {   
        $result = array();
        $exclude = array('.', '..', '.ds_store');
        if (is_readable($dir)){
            $root = scandir($dir);
            foreach($root as $value)
            {
                if(in_array(strtolower($value), $exclude)) continue;
                if(is_file($dir . DIRECTORY_SEPARATOR . $value)) {
                    $result[] = read_binary($dir . DIRECTORY_SEPARATOR . $value);
                    continue;
                }
                foreach(find_all_files($dir . DIRECTORY_SEPARATOR . $value) as $value)
                {
                    $result[] = $value;
                }
            }
        }
        return $result;
    }

    function read_binary($file){
        $handle = fopen($file, 'rb');
        $content = fread($handle, CHUNK_SIZE);
        
        ob_flush();
		flush();
        fclose($handle);
        
        return mb_convert_encoding($content, "UTF-8");
    }

    function find_duplicacy($path){
        $result = array_count_values(find_all_files($path));
        $result = array_filter($result, function($n){
            return $n > 1;
        });
        arsort($result);
        return $result;
    }

    function path_exist($folder)
    {
        // Get canonicalized absolute pathname
        $path = realpath($folder);
        // If it exist, check if it's a directory
        return ($path !== false AND is_dir($path)) ? $path : false;
    }

    function breadcrumb($path) {
        $entry = preg_split("/(\/|\\\\)/", $path);
        $entry = array_map(function ($e) {
            return '<li class="breadcrumb-item">' . $e . '</li>'; 
        }, $entry);
        if ($entry) $entry[count($entry) - 1] = preg_replace("/(breadcrumb-item)/", "$1 active", $entry[count($entry) - 1]);
        
        return implode("", $entry);
    }