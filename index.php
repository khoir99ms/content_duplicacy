<?php
	set_time_limit(0);
    ini_set('memory_limit', '1024M'); // or you could use 1G

    require_once("utils.php");
    require_once("profiler.php");

    $profiler = new Profiler();
    $profiler->timerStart();
    $holder = new ProfilerHolder( $profiler );

    $path = "./assets/raw/DropsuiteTest";
    $path = (!isset($_GET["path"]) || empty($_GET["path"])) ? $path : $_GET["path"];

    if (path_exist($path))
    {
        $path = path_exist($path);
        $find = find_duplicacy($path);
        $content = array_key_exists(0, array_keys($find)) ? array_keys($find)[0] : "no duplicacy found.";
        $duplicacy = array_key_exists(0, array_values($find)) ? array_values($find)[0] : 0;
    } else {
        $content = "Directory is not exist.";
        $duplicacy = 0;
    }
    $breadcrumb = breadcrumb($path);
?>
<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>Content Duplicacy</title>
        <!-- Bootstrap core CSS -->
        <link href="assets/css/bootstrap/bootstrap.min.css" rel="stylesheet">
        <!-- Custom styles for this template -->
        <link href="assets/css/custom.css" rel="stylesheet">
    </head>
    <body>
        <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
            <a class="navbar-brand" href="#">Content Duplicacy</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarCollapse">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item active">
                        <a class="nav-link" href="<?php echo $_SERVER["PHP_SELF"]; ?>">Home <span class="sr-only">(current)</span></a>
                    </li>
                </ul>
                <form method="get" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" class="form-inline mt-2 mt-md-0">
                    <input class="form-control mr-sm-2" type="text" name="path" placeholder="Directory" aria-label="Directory">
                    <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Find</button>
                </form>
            </div>
        </nav>
        <main role="main" class="container">
            <nav aria-label="breadcrumb" role="navigation">
                <ol class="breadcrumb bg-light">
                    <?php echo $breadcrumb; ?>
                </ol>
            </nav>
            
            <div class="card">
                <div class="card-body">
                    <p class="card-text">
                        <?php echo $content; ?>
                    </p>
                </div>
                <div class="card-footer text-muted">
                    <div class="float-left">
                        <?php echo $duplicacy . " Duplicacy"; ?>
                    </div>
                    <div class="float-right">
                        <?php echo (empty($content) ? "With empty content" : ""); ?>
                    </div>
                </div>
            </div>
        </main>
        <footer class="footer">
            <div class="container">
                <span class="text-muted">
                    <?php
                        echo "Elapsed time : " . $profiler->timerEnd();
                        unset($profiler);
                    ?>
                </span>
            </div>
        </footer>
    </body>
</html>