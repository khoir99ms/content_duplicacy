<?php
    function get_usage_in_kb(){
        echo memory_get_usage()/1024.0 . " kb \n";
    }

    class Profiler
    {
        protected $startTime = 0;
        public function timerStart(){
            $this->startTime = microtime(true);
        }
        function timerEnd(){
            return number_format( (microtime(true) - $this->startTime)*1000, 2 ) . ' ms';
        }
    }

    class ProfilerHolder {
        protected $profiler;
        public function __construct( Profiler $profiler ){
            $this->profiler = $profiler;
        }
    }